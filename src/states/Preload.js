import Phaser from 'phaser';
import sword from '../../assets/images/attack-icon.png';
import chest from '../../assets/images/chest-gold.png';
import quest from '../../assets/images/quest-button.png';
import coin from '../../assets/images/coin.png';
import potion from '../../assets/images/potion.png';
import shield from '../../assets/images/shield.png';
import scroll from '../../assets/images/scroll-skull.png';
import strangeItem from '../../assets/images/gods-helmet.png';
import monster from '../../assets/images/demon.png';
import dragon from '../../assets/images/goldendragon.png';
import snake from '../../assets/images/snake.png';
import skeleton from '../../assets/images/swordskeleton.png';
import tilesheet from '../../assets/images/terrains.png';
import player from '../../assets/images/player.png';
import myWorld from '../../assets/levels/myworld.json';

export default class Preload extends Phaser.State {
  preload() {
    this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bar');
    this.preloadBar.anchor.setTo(0.5);
    this.preloadBar.scale.setTo(100, 1);
    this.load.setPreloadSprite(this.preloadBar);

    this.load.image('sword', sword);
    this.load.image('quest', quest);
    this.load.image('chest', chest);
    this.load.image('coin', coin);
    this.load.image('potion', potion);
    this.load.image('shield', shield);
    this.load.image('scroll', scroll);
    this.load.image('strangeItem', strangeItem);
    this.load.image('monster', monster);
    this.load.image('dragon', dragon);
    this.load.image('snake', snake);
    this.load.image('skeleton', skeleton);
    this.load.image('tilesheet', tilesheet);

    this.load.spritesheet('player', player, 30, 30, 2, 0, 2);
    this.load.tilemap('myWorld', null, JSON.stringify(myWorld), Phaser.Tilemap.TILED_JSON);
  }
  create() {
    this.state.start('Game');
  }
}
