import Phaser from 'phaser';
import bar from '../../assets/images/preloader-bar.png';

export default class Boot extends Phaser.State {
  init() {
    this.game.stage.backgroundColor = '#fff';
    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    this.scale.pageAlignHorizontally = true;
    this.scale.pageAlignVertically = true;
    this.game.physics.startSystem(Phaser.Physics.ARCADE);
  }
  preload() {
    this.load.image('bar', bar);
  }
  create() {
    this.state.start('Preload');
  }
}
