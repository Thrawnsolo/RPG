import Phaser from 'phaser';

export default class Player extends Phaser.Sprite {
  constructor(state, x, y, data) {
    super(state.game, x, y, 'player');
    this.state = state;
    this.game = state.game;
    this.data = Object.create(data);
    this.anchor.setTo(0.5);
    this.animations.add('walk', [0, 1, 0], 6, false);
    this.healthBar = new Phaser.Sprite(state.game, this.x, this.y, 'bar');
    this.game.add.existing(this.healthBar);
    this.game.physics.arcade.enable(this.healthBar);
    this.healthBar.anchor.setTo(0.5);
    this.refreshHealthBar();
    this.game.physics.arcade.enable(this);
  }
  collectItem(item) {
    if (item.data.isQuest) {
      this.data.items.push(item);
      this.checkQuestCompletion(item);
    } else {
      this.data.health += item.data.health ? item.data.health : 0;
      this.data.attack += item.data.attack ? item.data.attack : 0;
      this.data.defense += item.data.defense ? item.data.defense : 0;
      this.data.gold += item.data.gold ? item.data.gold : 0;
      this.state.refreshStats();
      this.refreshHealthBar();
    }
    item.kill();
  }
  checkQuestCompletion(item) {
    let i = 0;
    let exitLoop = false;
    const numberQuests = this.data.quests.length;
    while (i < numberQuests && !exitLoop) {
      if (this.data.quests[i].code === item.data.questCode) {
        this.data.quests[i].isCompleted = true;
        exitLoop = true;
      }
      i += 1;
    }
  }
  refreshHealthBar() {
    this.healthBar.scale.setTo(this.data.health, 0.5);
  }
  update() {
    this.healthBar.x = this.x;
    this.healthBar.y = this.y - 25;
    this.healthBar.body.velocity = this.body.velocity;
  }
  kill() {
    super.kill();
    this.healthBar.kill();
  }
}
