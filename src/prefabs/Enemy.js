import Phaser from 'phaser';

export default class Enemy extends Phaser.Sprite {
  constructor(state, x, y, key, data) {
    super(state.game, x, y, key);

    this.state = state;
    this.game = state.game;
    this.data = Object.create(data);
    this.anchor.setTo(0.5);

    this.healthBar = new Phaser.Sprite(this.game, this.x, this.y, 'bar');
    this.game.add.existing(this.healthBar);
    this.healthBar.anchor.setTo(0.5);
    this.game.physics.arcade.enable(this);
    this.game.physics.arcade.enable(this.healthBar);
    this.refreshHealthBar();
    this.body.immovable = true;
  }
  refreshHealthBar() {
    this.healthBar.scale.setTo(this.data.health, 0.5);
  }
  update() {
    this.healthBar.x = this.x;
    this.healthBar.y = this.y - 25;
    this.healthBar.body.velocity = this.body.velocity;
  }
  kill() {
    super.kill();
    this.healthBar.kill();
  }
}
