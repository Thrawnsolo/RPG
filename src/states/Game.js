import Phaser from 'phaser';
import Player from '../prefabs/Player';
import OnscreenControls from '../plugins/OnscreenControls';
import Item from '../prefabs/Item';
import Enemy from '../prefabs/Enemy';
import Battle from '../prefabs/Battle';

const findObjectsByType = (targetType, tilemap, layer) => {
  const result = [];
  tilemap.objects[layer].forEach((elementObject) => {
    const element = elementObject;
    if (element.type === targetType) {
      element.y -= tilemap.tileHeight / 2;
      element.x += tilemap.tileWidth / 2;
      result.push(element);
    }
  });
  return result;
};

export default class Game extends Phaser.State {
  init(currentLevel) {
    this.currentLevel = currentLevel || 'myWorld';
    this.PLAYER_SPEED = 90;
    this.game.physics.arcade.gravity.y = 0;
    this.cursors = this.game.input.keyboard.createCursorKeys();
  }
  create() {
    this.game.onscreenControls = this.game.plugins.add(OnscreenControls);
    this.loadLevel();
  }
  update() {
    this.game.physics.arcade.collide(this.player, this.collisionLayer);

    this.game.physics.arcade.overlap(this.player, this.items, this.collectItem, null, this);

    this.player.body.velocity.x = 0;
    this.player.body.velocity.y = 0;
    if (!this.uiBlocked) {
      if (this.cursors.left.isDown || this.player.btnsPressed.left) {
        this.player.body.velocity.x = -this.PLAYER_SPEED;
        this.player.scale.setTo(1, 1);
      }
      if (this.cursors.right.isDown || this.player.btnsPressed.right) {
        this.player.body.velocity.x = this.PLAYER_SPEED;
        this.player.scale.setTo(-1, 1);
      }
      if (this.cursors.up.isDown || this.player.btnsPressed.up) {
        this.player.body.velocity.y = -this.PLAYER_SPEED;
      }
      if (this.cursors.down.isDown || this.player.btnsPressed.down) {
        this.player.body.velocity.y = this.PLAYER_SPEED;
      }
    }
    if (this.game.input.activePointer.isUp) {
      this.game.onscreenControls.stopMovement();
    }

    if (this.player.body.velocity.x !== 0 || this.player.body.velocity.y !== 0) {
      this.player.play('walk');
    } else {
      this.player.animations.stop();
      this.player.frame = 0;
    }

    this.game.physics.arcade.collide(this.player, this.enemies, this.attack, null, this);
  }
  attack(playerElement, enemy) {
    const player = playerElement;
    this.battle.attack(player, enemy);
    this.battle.attack(enemy, player);
    // bounce the player back
    if (player.body.touching.up) {
      player.y += 15;
    }
    if (player.body.touching.down) {
      player.y -= 15;
    }
    if (player.body.touching.left) {
      player.x += 15;
    }
    if (player.body.touching.right) {
      player.x -= 15;
    }
    if (player.data.health <= 0) {
      this.gameOver();
    }
  }
  loadLevel() {
    const playerData = {
      items: [],
      health: 25,
      attack: 12,
      defense: 8,
      gold: 100,
      quests: [
        {
          name: 'Find the Magic Scroll',
          code: 'magic-scroll',
          isCompleted: false
        }, {
          name: 'Find the Helmet of the Gods',
          code: 'gods-helmet',
          isCompleted: false
        }
      ]
    };
    this.map = this.add.tilemap(this.currentLevel);
    this.map.addTilesetImage('terrains', 'tilesheet');

    this.backgroundLayer = this.map.createLayer('background');
    this.collisionLayer = this.map.createLayer('collision');

    this.game.world.sendToBack(this.backgroundLayer);
    this.map.setCollisionBetween(1, 16, true, 'collision');

    this.collisionLayer.resizeWorld();

    this.player = new Player(this, 556, 361, playerData);

    this.add.existing(this.player);

    this.items = this.add.group();
    this.enemies = this.add.group();
    this.initGUI();

    this.loadItems();
    // this.enemy = new Enemy(this, 556, 311, 'monster', { attack: 10, health: 20, defense: 5 });
    // this.enemies.add(this.enemy);
    this.loadEnemies();

    this.battle = new Battle(this.game);

    this.game.camera.follow(this.player);
  }
  initGUI() {
    this.game.onscreenControls.setup(this.player, {
      left: true,
      right: true,
      up: true,
      down: true,
      upleft: true,
      downleft: true,
      upright: true,
      downright: true,
      action: true
    });

    this.showPlayerIcons();
  }
  gameOver() {
    this.game.state.start('Game', true, false, this.currentLevel);
  }
  collectItem(player, item) {
    this.player.collectItem(item);
  }
  showPlayerIcons() {
    const style = { font: '14px Arial', fill: '#fff' };
    this.goldIcon = this.add.sprite(10, 10, 'coin');
    this.goldIcon.fixedToCamera = true;
    this.goldLabel = this.add.text(32, 12, '0', style);
    this.goldLabel.fixedToCamera = true;

    this.attackIcon = this.add.sprite(70, 10, 'sword');
    this.attackIcon.fixedToCamera = true;
    this.attackLabel = this.add.text(92, 12, '0', style);
    this.attackLabel.fixedToCamera = true;

    this.defenseIcon = this.add.sprite(130, 12, 'shield');
    this.defenseIcon.fixedToCamera = true;
    this.defenseLabel = this.add.text(152, 12, '0', style);
    this.defenseLabel.fixedToCamera = true;

    this.refreshStats();

    this.questIcon = this.add.sprite(this.game.width - 30, 10, 'quest');
    this.questIcon.fixedToCamera = true;

    this.overlay = this.add.bitmapData(this.game.width, this.game.height);
    this.overlay.ctx.fillStyle = '#000';
    this.overlay.ctx.fillRect(0, 0, this.game.width, this.game.height);

    this.questsPanelGroup = this.add.group();
    this.questsPanelGroup.y = this.game.height;
    this.questsPanel = new Phaser.Sprite(this.game, 0, 0, this.overlay);
    this.questsPanel.alpha = 0.8;
    this.questsPanel.fixedToCamera = true;
    this.questsPanelGroup.add(this.questsPanel);

    // content of the panel
    this.questInfo = new Phaser.Text(this.game, 50, 50, '', style);
    this.questInfo.fixedToCamera = true;
    this.questsPanelGroup.add(this.questInfo);

    // show quest panel when touched
    this.questIcon.inputEnabled = true;
    this.questIcon.events.onInputDown.add(this.showQuests, this);

    // hide quest panel when touched
    this.questsPanel.inputEnabled = true;
    this.questsPanel.events.onInputDown.add(this.hideQuests, this);
  }
  refreshStats() {
    this.goldLabel.text = this.player.data.gold;
    this.attackLabel.text = this.player.data.attack;
    this.defenseLabel.text = this.player.data.defense;
  }
  loadItems() {
    const elementsArray = findObjectsByType('item', this.map, 'object');
    elementsArray.forEach((elementObject) => {
      const element = new Item(
        this,
        elementObject.x,
        elementObject.y,
        elementObject.properties.asset,
        elementObject.properties
      );
      this.items.add(element);
    });
  }
  loadEnemies() {
    const elementsArray = findObjectsByType('enemy', this.map, 'object');
    elementsArray.forEach((elementObject) => {
      const element = new Enemy(
        this,
        elementObject.x,
        elementObject.y,
        elementObject.properties.asset,
        elementObject.properties
      );
      this.enemies.add(element);
    });
  }
  showQuests() {
    // player cant move
    this.uiBlocked = true;
    const showPanelTween = this.add.tween(this.questsPanelGroup);
    showPanelTween.to({ y: 0 }, 150);
    showPanelTween.onComplete.add(() => {
      let questsText = 'QUESTS\n';
      this.player.data.quests.forEach((quest) => {
        questsText += `${quest.name}${quest.isCompleted ? ' - DONE' : ''}\n`;
      });
      this.questInfo.text = questsText;
    });
    showPanelTween.start();
  }
  hideQuests() {
    this.uiBlocked = false;
    const hidePanelTween = this.add.tween(this.questsPanelGroup);
    hidePanelTween.to({ y: this.game.height }, 150);
    hidePanelTween.onComplete.add(() => {
      this.uiBlocked = false;
      this.questInfo.text = '';
    });
    hidePanelTween.start();
  }
}
