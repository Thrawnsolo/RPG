export default class Battle {
  constructor(game) {
    this.game = game;
  }
  attack(attacker, attacked) {
    const elementToBeAttacked = attacked;
    const damage = Math.max(
      0,
      (attacker.data.attack * Math.random()) - (elementToBeAttacked.data.defense * Math.random())
    );
    elementToBeAttacked.data.health -= damage;
    attacked.refreshHealthBar();

    const attackedTween = this.game.add.tween(elementToBeAttacked);
    attackedTween.to({ tint: 0xFF0000 }, 200);
    attackedTween.onComplete.add(() => {
      elementToBeAttacked.tint = 0xFFFFFF;
    });
    attackedTween.start();

    if (elementToBeAttacked.data.health <= 0) {
      elementToBeAttacked.kill();
    }
  }
}
