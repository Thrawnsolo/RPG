/* eslint-disable-next-line */
import pixi from 'pixi';
/* eslint-disable-next-line */
import p2 from 'p2';
import Phaser from 'phaser';
import bootState from './states/Boot';
import preloadState from './states/Preload';
import gameState from './states/Game';

class Main extends Phaser.Game {
  static getDimensions(maxWidth, maxHeight) {
    const width = window.innerWidth * window.devicePixelRatio;
    const height = window.innerHeight * window.devicePixelRatio;
    let landscapeWidth = Math.max(width, height);
    let landscapeHeight = Math.min(width, height);

    if (landscapeWidth > maxWidth) {
      const ratioWidth = maxWidth / landscapeWidth;
      landscapeWidth *= ratioWidth;
      landscapeHeight *= ratioWidth;
    }
    if (landscapeHeight > maxHeight) {
      const ratioHeight = maxHeight / landscapeHeight;
      landscapeWidth *= ratioHeight;
      landscapeHeight *= ratioHeight;
    }
    return { width: landscapeWidth, height: landscapeHeight };
  }

  constructor() {
    const dimensions = Main.getDimensions(1280, 768);
    super(dimensions.width, dimensions.height, Phaser.AUTO);
    this.state.add('Boot', bootState);
    this.state.add('Preload', preloadState);
    this.state.add('Game', gameState);
  }
}

window.game = new Main();
window.game.state.start('Boot');
