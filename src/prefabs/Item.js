import Phaser from 'phaser';

export default class Item extends Phaser.Sprite {
  constructor(state, x, y, key, data) {
    super(state.game, x, y, key);

    this.state = state;
    this.game = state.game;
    this.data = Object.create(data);
    this.anchor.setTo(0.5);

    this.game.physics.arcade.enable(this);
  }
}
