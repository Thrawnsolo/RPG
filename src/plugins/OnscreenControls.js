import Phaser from 'phaser';

export default class onscreenControls extends Phaser.Plugin {
  constructor(game, parent) {
    super(game, parent);
    this.game = game;
  }

  setUpButton(positionMap, key, alphaMap, angleMap, directionMap) {
    const position = positionMap[key];
    const bitmap = key === 'action' ? this.actionBitmap : this.directionBitmap;
    if (key !== 'action') {
      this.setBasicProperties(`${key}Arrow`, key, position, bitmap, alphaMap, angleMap);

      this[`${key}Arrow`].events.onInputDown.add(() => {
        this.setButtonPressedStatus(directionMap, key, true);
      });

      this[`${key}Arrow`].events.onInputUp.add(() => {
        this.setButtonPressedStatus(directionMap, key, false);
      });

      this[`${key}Arrow`].events.onInputOver.add(() => {
        this.setButtonPressedStatus(directionMap, key, true);
      });

      this[`${key}Arrow`].events.onInputOut.add(() => {
        this.setButtonPressedStatus(directionMap, key, false);
      });
    } else {
      this.setBasicProperties(`${key}Button`, key, position, bitmap, alphaMap, angleMap);

      this[`${key}Button`].events.onInputDown.add(() => {
        this.player.btnsPressed[`${key}`] = true;
      });
      this[`${key}Button`].events.onInputUp.add(() => {
        this.player.btnsPressed[`${key}`] = false;
      });
    }
  }

  setBasicProperties(elementName, key, position, bitmap, alphaMap, angleMap) {
    const button = this.game.add.button(position.x, position.y, bitmap);
    button.alpha = alphaMap[key];
    button.fixedToCamera = true;
    button.angle = angleMap[key];
    this[elementName] = button;
  }

  setButtonPressedStatus(directionMap, key, value) {
    directionMap[key].forEach((direction) => {
      this.player.btnsPressed[direction] = value;
    });
  }

  setup(player, buttons) {
    this.player = player;
    this.player.btnsPressed = this.player.btnsPressed || {};
    // Feel free to change it at your desire
    this.btnHeight = 0.08 * this.game.width;
    this.btnWidth = 0.08 * this.game.width;
    this.edgeDistance = 0.25 * this.btnHeight;
    this.sizeActionButton = 1.5 * this.btnHeight;

    const leftX = this.edgeDistance;
    const leftY = this.game.height - this.edgeDistance - this.btnWidth - this.btnHeight;

    const rightX = this.edgeDistance + this.btnHeight + this.btnWidth;
    const rightY = this.game.height - this.edgeDistance - this.btnWidth - this.btnHeight;

    const upX = this.edgeDistance + this.btnHeight + this.btnWidth;
    const upY = this.game.height - this.edgeDistance - (2 * this.btnWidth) - this.btnHeight;

    const downX = this.edgeDistance + this.btnHeight + this.btnWidth;
    const downY = this.game.height - this.edgeDistance - this.btnWidth;

    const actionX = this.game.width - this.edgeDistance - this.sizeActionButton;
    const actionY = this.game.height - this.edgeDistance - this.btnWidth - this.btnHeight;

    const alphaMap = {
      up: 0.5,
      left: 0.5,
      right: 0.5,
      down: 0.5,
      upleft: 0.3,
      upright: 0.3,
      downleft: 0.3,
      downright: 0.3,
      action: 0.5
    };
    const angleMap = {
      up: 90,
      left: 0,
      right: 0,
      down: 90,
      upleft: 0,
      upright: 0,
      downleft: 0,
      downright: 0,
      action: 0
    };
    const positionMap = {
      up: {
        x: upX,
        y: upY
      },
      left: {
        x: leftX,
        y: leftY
      },
      right: {
        x: rightX,
        y: rightY
      },
      down: {
        x: downX,
        y: downY
      },
      upleft: {
        x: leftX,
        y: upY
      },
      upright: {
        x: rightX,
        y: upY
      },
      downleft: {
        x: leftX,
        y: downY
      },
      downright: {
        x: rightX,
        y: downY
      },
      action: {
        x: actionX,
        y: actionY
      }
    };
    const directionMap = {
      up: ['up'],
      left: ['left'],
      right: ['right'],
      down: ['down'],
      upleft: ['up', 'left'],
      upright: ['up', 'right'],
      downleft: ['down', 'left'],
      downright: ['down', 'right'],
      action: ['action']
    };

    this.directionBitmap = this.game.add.bitmapData(this.btnWidth, this.btnHeight);
    this.directionBitmap.ctx.fillStyle = '#4BAFE3';
    this.directionBitmap.ctx.fillRect(0, 0, this.btnWidth, this.btnHeight);

    this.diagonalBitmap = this.game.add.bitmapData(this.btnWidth, this.btnHeight);
    this.diagonalBitmap.ctx.fillStyle = '#4BAFE3';
    this.diagonalBitmap.ctx.fillRect(0, 0, this.btnWidth, this.btnHeight);

    this.actionBitmap = this.game.add.bitmapData(this.btnWidth, this.btnHeight);
    this.actionBitmap.ctx.fillStyle = '#FF0000';
    this.actionBitmap.ctx.fillRect(0, 0, this.btnWidth, this.btnHeight);

    Object.keys(buttons).forEach((key) => {
      if (buttons[key]) {
        this.setUpButton(positionMap, key, alphaMap, angleMap, directionMap);
      }
    });
  }

  stopMovement() {
    this.player.btnsPressed = {};
  }
}
